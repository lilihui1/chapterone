import function.MyConsumer;
import function.MyFunction;
import function.MyPredicate;
import function.MySupplier;

import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * @ClassName MyOptional
 * @Description MyOptional
 * @Author ASUS
 * @Date 2021/12/19 14:39
 * @Version 1.0
 */
public final class MyOptional<T> {

    private static final MyOptional<?> EMPTY = new MyOptional<>();


    private final T value;


    private MyOptional() {
        this.value = null;
    }


    public static<T> MyOptional<T> empty() {
        @SuppressWarnings("unchecked")
        MyOptional<T> t = (MyOptional<T>) EMPTY;
        return t;
    }


    private MyOptional(T value) {
        this.value = Objects.requireNonNull(value);
    }


    public static <T> MyOptional<T> of(T value) {
        return new MyOptional<>(value);
    }


    public static <T> MyOptional<T> ofNullable(T value) {
        return value == null ? empty() : of(value);
    }


    public T get() {
        if (value == null) {
            throw new NoSuchElementException("No value present");
        }
        return value;
    }


    public boolean isPresent() {
        return value != null;
    }


    public void ifPresent(MyConsumer<? super T> MyConsumer) {
        if (value != null)
            MyConsumer.accept(value);
    }


    public MyOptional<T> filter(MyPredicate<? super T> MyPredicate) {
        Objects.requireNonNull(MyPredicate);
        if (!isPresent())
            return this;
        else
            return MyPredicate.test(value) ? this : empty();
    }


    public<U> MyOptional<U> map(MyFunction<? extends U ,? super T> mapper) {
        Objects.requireNonNull(mapper);
        if (!isPresent())
            return empty();
        else {
            return MyOptional.ofNullable(mapper.apply(value));
        }
    }


    public<U> MyOptional<U> flatMap(MyFunction<MyOptional<U>, ? super T> mapper) {
        Objects.requireNonNull(mapper);
        if (!isPresent())
            return empty();
        else {
            return Objects.requireNonNull(mapper.apply(value));
        }
    }


    public T orElse(T other) {
        return value != null ? value : other;
    }


    public T orElseGet(MySupplier<? extends T> other) {
        return value != null ? value : other.get();
    }


    public <X extends Throwable> T orElseThrow(MySupplier<? extends X> exceptionSupplier) throws X {
        if (value != null) {
            return value;
        } else {
            throw exceptionSupplier.get();
        }
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MyOptional)) {
            return false;
        }

        MyOptional<?> other = (MyOptional<?>) obj;
        return Objects.equals(value, other.value);
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }


    @Override
    public String toString() {
        return value != null
                ? String.format("MyOptional[%s]", value)
                : "MyOptional.empty";
    }
}

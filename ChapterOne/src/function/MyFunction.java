package function;

@FunctionalInterface
public interface MyFunction<R, T> {
    /**
     * 函数式接口
     * 类似于 y = F(x)
     * */
    R apply(T t);
}

package function;

@FunctionalInterface
public interface MyBiFunction<R, T, U> {
    /**
     * 函数式接口
     * 类似于 z = F(x,y)
     * */
    R apply(T t, U u);
}

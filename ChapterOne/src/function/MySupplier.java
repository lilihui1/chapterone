package function;

@FunctionalInterface
public interface MySupplier<T> {
    T get();
}

import function.MyConsumer;
import function.MyFunction;
import function.MyPredicate;
import function.MySupplier;

/**
 * @ClassName MyCustomInterfaceApp
 * @Description 自定义接口使用实例类
 * @Author ASUS
 * @Date 2021/12/19 14:26
 * @Version 1.0
 */
public class MyCustomInterfaceApp {
    public static void main(String[] args){
        myFunctionApp();
        myConsumerApp();
        myPredicateApp();
        mySupplierApp();
    }


    static void myFunctionApp(){
        int myNumber = 10;
        // 使用lambda表达式实现函数式接口
        int res1 = modifyTheValue(myNumber, (x)-> x + 20);
        System.out.println(res1); // 30
        //  使用匿名内部类实现
        int res2 = modifyTheValue(myNumber, new MyFunction<Integer, Integer>() {
            @Override
            public Integer apply(Integer t) {
                return t + 20;
            }
        });
        System.out.println(res2); // 30
    }
    static int modifyTheValue(int valueToBeOperated, MyFunction<Integer, Integer> function) {
        return function.apply(valueToBeOperated);
    }

    static void myConsumerApp(){
        modifyTheValue3(3, (x) -> System.out.println(x * 2));
    }
    static void modifyTheValue3(int value, MyConsumer<Integer> consumer) {
        consumer.accept(value);
    }
    static void myPredicateApp(){
        System.out.println(predicateTest(3, (x) -> x == 3));
    }
    static boolean predicateTest(int value, MyPredicate<Integer> predicate) {
        return predicate.test(value);
    }

    static void mySupplierApp(){
        String name = "啦啦啦啦啦";
        // () -> name.length() 无参数，返回一个结果（字符串长度）
        // 所以该lambda表达式可以实现Supplier接口
        System.out.println(supplierTest(() -> name.length() + ""));
    }
    static String supplierTest(MySupplier<String> supplier) {
        return supplier.get();
    }


}

import function.EvalFunction;

/**
 * @ClassName NextItemEvalProcess
 * @Description 下一个元素求值过程
 * @Author ASUS
 * @Date 2021/12/19 15:19
 * @Version 1.0
 */
public class NextItemEvalProcess {
    /**
     * 求值方法
     * */
    private EvalFunction evalFunction;

    public NextItemEvalProcess(EvalFunction evalFunction) {
        this.evalFunction = evalFunction;
    }

    MyStream eval(){
        return evalFunction.apply();
    }
}

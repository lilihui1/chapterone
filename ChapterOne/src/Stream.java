import function.MyBiFunction;
import function.MyFunction;
import function.MyPredicate;

/**
 * @ClassName Stream
 * @Description Stream
 * @Author ASUS
 * @Date 2021/12/19 14:47
 * @Version 1.0
 */
public interface Stream<T>{
    /**
     * 映射 lazy 惰性求值
     * @param mapper 转换逻辑 T->R
     * @return 一个新的流
     * */
    <R> MyStream<R> map(MyFunction<R,T> mapper);

    /**
     * 扁平化 映射 lazy 惰性求值
     * @param mapper 转换逻辑 T->MyStream<R>
     * @return  一个新的流(扁平化之后)
     * */
    <R> MyStream<R> flatMap(MyFunction<? extends MyStream<R>, T> mapper);

    /**
     * 过滤 lazy 惰性求值
     * @param predicate 谓词判断
     * @return 一个新的流，其中元素是满足predicate条件的
     * */
    MyStream<T> filter(MyPredicate<T> predicate);
    /**
     * 浓缩 eval 强制求值
     * @param initVal 浓缩时的初始值
     * @param accumulator 浓缩时的 累加逻辑
     * @return 浓缩之后的结果
     * */
    <R> R reduce(R initVal, MyBiFunction<R, R, T> accumulator);

    /**
     * 收集 eval 强制求值
     * @param collector 传入所需的函数组合子，生成高阶函数
     * @return 收集之后的结果
     * */
    <R, A> R collect(Collector<T,A,R> collector);
    /**
     * 返回空的 stream
     * @return 空stream
     * */
    static <T> MyStream<T> makeEmptyStream(){
        // isEnd = true
        return new MyStream.Builder<T>().isEnd(true).build();
    }
}

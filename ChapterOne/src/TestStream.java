import java.util.List;

/**
 * @ClassName TestStream
 * @Description TODO
 * @Author ASUS
 * @Date 2021/12/19 16:26
 * @Version 1.0
 */
public class TestStream {
    public static void main(String[] args){
        // 生成整数流 1-10
        Stream<Integer> intStream = IntegerStreamGenerator.getIntegerStream(1,10);

        // intStream基础上过滤出偶数
        Stream<Integer> filterStream =  intStream.filter(item-> item%2 == 0);

        // filterStream基础上映射为平方
        Stream<Integer> mapStream = filterStream.map(item-> item * item);


        // 最终结果累加求和(初始值为0)
        Integer sum = mapStream.reduce(0,(i1,i2)-> i1+i2);

        System.out.println(sum); // 20

        //以上的步骤结合就是：
        Integer sum2 = IntegerStreamGenerator.getIntegerStream(1,10)
                .filter(item -> item%2 == 0) // 过滤出偶数
                .map(item-> item * item)    // 映射为平方
                .reduce(0,(i1,i2)-> i1+i2); // 最终结果累加求和(初始值为0)

        System.out.println(sum2); // 220

        List<Integer> list = IntegerStreamGenerator.getIntegerStream(1,10)
                .filter(item -> item%2 == 0).collect(CollectUtils.toList());
        System.out.println(list);
    }
}

import function.MyBiFunction;
import function.MyFunction;
import function.MySupplier;

/**
 * collect接口 收集器
 * 通过传入组合子，生成高阶过程
 */
public interface Collector<T, A, R> {

    /**
     * 收集时，提供初始化的值
     * */
    MySupplier<A> supplier();

    /**
     * A = A + T
     * 累加器，收集时的累加过程
     * */
    MyBiFunction<A, A, T> accumulator();

    /**
     * 收集完成之后的收尾操作
     * */
    MyFunction<R, A> finisher();
}